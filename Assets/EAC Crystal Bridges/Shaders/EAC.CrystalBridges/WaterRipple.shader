﻿Shader "EAC.CrystalBridges/WaterRipple"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _WaveCount ("Number of waves", Int) = 0
        _UVAspect ("AspectRatio of UV space", Float) = 1.0
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #define EAC_MAX_WAVE_COUNT 6

        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
        int _WaveCount;
        float _UVAspect;
        // xy: center in UV, z: inner radius, w: outer radius
        float4 _WaveCenterRadii[EAC_MAX_WAVE_COUNT];
        // x: height of wave
        float4 _WaveScale[EAC_MAX_WAVE_COUNT];

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        float2 calcWaveOffsetUV(in float2 baseUV, in int waveIdx)
        {
            if (waveIdx >= _WaveCount)
                return baseUV;

            float2 center = _WaveCenterRadii[waveIdx].xy;
            float2 radii = _WaveCenterRadii[waveIdx].zw;

            // square of inner/outer radii
            float2 radii2 = radii * radii;
            // delta from center to baseUV
            float2 uvDelta = baseUV - center;
            uvDelta.y = uvDelta.y * _UVAspect;
            // length of delta squared
            float dist2 = dot(uvDelta, uvDelta);

            // point is outside [innerR, outerR]?
            if (dist2 < radii2.x || dist2 > radii2.y)
                return baseUV;

            // map dist -> [0, 1]
            float t = (dist2 - radii2.x) / (radii2.y - radii2.x);
            // map t -> [-1.5, 1.5]
            float phase = 5.0 * t - 2.5;
            float cos_t = cos(UNITY_PI * phase);
            float scale = _WaveScale[waveIdx].x;

            return baseUV + scale * cos_t * uvDelta;
        }

        float2 calcAllWaveOffsetsUV(in float2 baseUV)
        {
            float2 offsetUV = float2(0.0, 0.0);

            if (_WaveCount > 0)
            {
                for (int i = 0; i < EAC_MAX_WAVE_COUNT; ++i)
                    offsetUV += calcWaveOffsetUV(baseUV, i);

                offsetUV /= EAC_MAX_WAVE_COUNT;
            }
            else
            {
                offsetUV = baseUV;
            }

            return offsetUV;
        }

        float2 calcBaseRipple(in float2 baseUV)
        {
            float amplitude = 0.003 * cos(baseUV.y);
            float u = baseUV.x + amplitude * sin((0.3 * _Time.y + baseUV.y) * UNITY_PI);

            return float2(u, baseUV.y);
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            float2 rippleUV = calcBaseRipple(IN.uv_MainTex);
            float2 uv = calcAllWaveOffsetsUV(rippleUV);

            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, uv) * _Color;
            // fixed4 c = fixed4(uv.x, uv.y, 0.0, 1.0);
            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
