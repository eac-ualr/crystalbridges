﻿Shader "EAC.CrystalBridges/DepthWriteOnly"
{
    Properties
    {
    }

    SubShader
    {
        // This shader must run between the Vuforia/VideoBackground shader (which
        // uses QueueGeometry-11) and the opaque scene geometry
        Tags { "RenderType"="Opaque" "Queue"="Geometry-5"}
        LOD 100

        Pass
        {
            ZTest Always
            ColorMask 0

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                return o;
            }

            void frag (v2f i, out fixed4 color : SV_Target)
            {
                color = fixed4(0.0, 0.0, 0.0, 1.0);
            }
            ENDCG
        }
    }
}
