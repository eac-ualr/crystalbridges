
namespace EAC.CrystalBridges
{
    // unity namespaces
    using UnityEngine;
    // external namespaces
    using Vuforia;


    public class TrackableEventHandler
        : MonoBehaviour
        , ITrackableEventHandler
    {
        public GameObject augmentationObject;
        public AudioSource[] audioSources;

        private TrackableBehaviour m_trackableBehaviour;
        private TrackableBehaviour.Status m_prevStatus;
        private TrackableBehaviour.Status m_currStatus;

        // ------------------------------------------------------------------
        #region MonoBehaviour

        private void Awake()
        {
            m_trackableBehaviour = GetComponent<TrackableBehaviour>();
            m_prevStatus = TrackableBehaviour.Status.NO_POSE;
            m_currStatus = TrackableBehaviour.Status.NO_POSE;
        }

        private void OnEnable()
        {
            if (m_trackableBehaviour != null)
                m_trackableBehaviour.RegisterTrackableEventHandler(this);
        }

        private void OnDisable()
        {
            if (m_trackableBehaviour != null)
                m_trackableBehaviour.UnregisterTrackableEventHandler(this);
        }

        #endregion MonoBehaviour
        // ------------------------------------------------------------------
        #region ITrackableEventHandler

        public void OnTrackableStateChanged(
            TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
        {
            m_prevStatus = previousStatus;
            m_currStatus = newStatus;

            if (newStatus == TrackableBehaviour.Status.DETECTED
                || newStatus == TrackableBehaviour.Status.TRACKED
                || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTargetFound();
            }
            else if (previousStatus == TrackableBehaviour.Status.TRACKED
                && newStatus == TrackableBehaviour.Status.NO_POSE)
            {
                OnTargetLost();
            }
            else
            {
                OnTargetLost();
            }
        }

        #endregion ITrackableEventHandler
        // ------------------------------------------------------------------

        private void OnTargetFound()
        {
            Debug.Log("[TrackableEventHandler.OnTargetFound]");
            augmentationObject.SetActive(true);

            for (int i = 0, iEnd = audioSources.Length; i < iEnd; ++i)
            {
                audioSources[i].Play();
            }
        }

        private void OnTargetLost()
        {
            Debug.Log("[TrackableEventHandler.OnTargetLost]");
            augmentationObject.SetActive(false);

            for (int i = 0, iEnd = audioSources.Length; i < iEnd; ++i)
            {
                audioSources[i].Pause();
            }
        }
    }

} // namespace EAC.CrystalBridges
