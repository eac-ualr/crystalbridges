
namespace EAC.CrystalBridges
{
    // system namespaces
    using System.Collections.Generic;
    // unity namespaces
    using UnityEngine;
    using UnityEngine.Assertions;
    // local namespaces
    using EAC.Utility;


    public class WaterRippleBehaviour
        : MonoBehaviour
    {
        public MeshRenderer targetRenderer;

        public AnimationCurve innerRadiusCurve;
        public AnimationCurve outerRadiusCurve;
        public AnimationCurve scaleCurve;

        private Material m_material;
        private List<WaveInfo> m_waves = new List<WaveInfo>();
        private ObjectPool<WaveInfo> m_wavePool = new ObjectPool<WaveInfo>();

        // arrays to be passed to shader
        private Vector4[] m_waveCenterRadii = new Vector4[kMaxWaveCount];
        private Vector4[] m_waveScale = new Vector4[kMaxWaveCount];

        private int m_paramWaveCenterRadii;
        private int m_paramWaveScale;
        private int m_paramWaveCount;

        private class WaveInfo
        {
            public Vector2 center;
            public float startTime;
        }

        // Keep this value in sync with EAC_MAX_WAVE_COUNT in WaterRipple.shader!
        private const int kMaxWaveCount = 6;

        public void StartWave(Vector2 uvCenter)
        {
            WaveInfo waveInfo = m_wavePool.Acquire();
            waveInfo.startTime = Time.time;
            waveInfo.center = uvCenter;

            AddWaveInfo(waveInfo);
        }

        // ------------------------------------------------------------------
        #region MonoBehaviour

        private void Awake()
        {
            Assert.IsNotNull(targetRenderer);
            m_material = targetRenderer.material;

            m_paramWaveCenterRadii = Shader.PropertyToID("_WaveCenterRadii");
            m_paramWaveScale = Shader.PropertyToID("_WaveScale");
            m_paramWaveCount = Shader.PropertyToID("_WaveCount");
        }

        private void Update()
        {
            UpdateProperties();
        }

        #endregion MonoBehaviour
        // ------------------------------------------------------------------

        private void UpdateProperties()
        {
            for (int i = 0; i < m_waves.Count;)
            {
                WaveInfo waveInfo = m_waves[i];

                float deltaT = Time.time - waveInfo.startTime;
                float innerRadius = innerRadiusCurve.Evaluate(deltaT);
                float outerRadius = outerRadiusCurve.Evaluate(deltaT);
                float scale = scaleCurve.Evaluate(deltaT);

                Vector4 waveCenterRadii = new Vector4(
                    waveInfo.center.x, waveInfo.center.y, innerRadius, outerRadius);
                Vector4 waveScale = new Vector4(scale, 0.0f, 0.0f, 0.0f);

                m_waveCenterRadii[i] = waveCenterRadii;
                m_waveScale[i] = waveScale;

                if (innerRadius >= 1.0f)
                {
                    m_wavePool.Release(m_waves[i]);
                    m_waves.RemoveAt(i);
                }
                else
                {
                    ++i;
                }
            }

            m_material.SetVectorArray(m_paramWaveCenterRadii, m_waveCenterRadii);
            m_material.SetVectorArray(m_paramWaveScale, m_waveScale);
            m_material.SetInt(m_paramWaveCount, m_waves.Count);
        }

        private void AddWaveInfo(WaveInfo waveInfo)
        {
            if (m_waves.Count < kMaxWaveCount)
            {
                m_waves.Add(waveInfo);
            }
            else
            {
                float oldestStartTime = Time.time;
                int oldestWaveIdx = -1;

                for (int i = 0, iEnd = m_waves.Count; i < iEnd; ++i)
                {
                    if (m_waves[i].startTime < oldestStartTime)
                    {
                        oldestStartTime = m_waves[i].startTime;
                        oldestWaveIdx = i;
                    }
                }

                WaveInfo oldestWaveInfo = m_waves[oldestWaveIdx];
                m_wavePool.Release(oldestWaveInfo);
                m_waves[oldestWaveIdx] = waveInfo;
            }
        }
    }


} // namespace EAC.CrystalBridges
