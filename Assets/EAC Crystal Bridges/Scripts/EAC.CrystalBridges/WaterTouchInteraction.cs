
namespace EAC.CrystalBridges
{
    // unity namespaces
    using UnityEngine;
    using UnityEngine.Assertions;
    // local namespaces
    using DebugDraw = EAC.Utility.DebugDraw.DebugDraw;
    using Layers = EAC.CrystalBridges.Constants.Layers;


    public class WaterTouchInteraction
        : MonoBehaviour
    {
        [Tooltip("Camera used to find position where water was touched (default: Main Camera)")]
        public new Camera camera;
        [Tooltip("Water Ripple Behaviour (default: from same GameObject)")]
        public WaterRippleBehaviour waterRipple;
        [Tooltip("Array of clips to play on touch (randomly chosen)")]
        public AudioClip[] waterSFX;

        private AudioSource[] m_audioSources;

        // ------------------------------------------------------------------
        #region MonoBehaviour

        private void Awake()
        {
            if (camera == null)
                camera = Camera.main;

            if (waterRipple == null)
                waterRipple = GetComponent<WaterRippleBehaviour>();
            Assert.IsNotNull(waterRipple);

            m_audioSources = new AudioSource[waterSFX.Length];
            for (int i = 0, iEnd = waterSFX.Length; i < iEnd; ++i)
            {
                m_audioSources[i] = gameObject.AddComponent<AudioSource>();
                m_audioSources[i].clip = waterSFX[i];
                m_audioSources[i].playOnAwake = false;
            }
        }

        private void Update()
        {
            if (Input.touchSupported)
            {
                ProcessTouchEvents();
            }
            else
            {
                ProcessMouseEvents();
            }
        }

        #endregion MonoBehaviour
        // ------------------------------------------------------------------

        private void ProcessTouchEvents()
        {
            for (int i = 0, iEnd = Input.touchCount; i < iEnd; ++i)
            {
                Touch touch = Input.GetTouch(i);

                if (touch.phase == TouchPhase.Began)
                {
                    Vector3 touchPosition = new Vector3(touch.position.x, touch.position.y, 0.0f);
                    ProcessRay(touchPosition);
                }
            }
        }

        private void ProcessMouseEvents()
        {
            if (Input.GetMouseButtonDown(0))
            {
                ProcessRay(Input.mousePosition);
            }
        }

        private void ProcessRay(Vector3 screenPosition)
        {
            Ray ray = camera.ScreenPointToRay(screenPosition);
            RaycastHit hitInfo;
            bool hit = Physics.Raycast(
                ray, out hitInfo, Mathf.Infinity, Layers.maskWater, QueryTriggerInteraction.Ignore);

            if (hit)
            {
#if UNITY_EDITOR
                DebugDraw.DrawWireCube(hitInfo.point, 0.05f * Vector3.one, Color.red, 1.0f);
#endif

                int audioIdx = Random.Range(0, waterSFX.Length);
                if (!m_audioSources[audioIdx].isPlaying)
                {
                    m_audioSources[audioIdx].Play();
                }

                waterRipple.StartWave(hitInfo.textureCoord);
            }
        }
    }


} // namespace EAC.CrystalBridges
