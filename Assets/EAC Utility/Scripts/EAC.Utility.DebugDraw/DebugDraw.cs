
namespace EAC.Utility.DebugDraw
{
    // system namespaces
    using ConditionalAttribute = System.Diagnostics.ConditionalAttribute;
    // unity namespaces
    using UnityEngine;


    public static class DebugDraw
    {
        public delegate string TextFunc();

        public static Camera debugDrawCamera;

        // ----------------------------------------------------------------------

        private static DebugDrawHelper m_helper = null;

        private const string kHelperGOName = "EAC.DebugDraw.DebugDrawHelper";

        // ----------------------------------------------------------------------

        public static Camera GetDebugDrawCamera()
        {
            return debugDrawCamera != null ? debugDrawCamera : Camera.main;
        }

        public static void DrawLine(
            Vector3 startPnt, Vector3 endPnt, Color color, float duration = 0.0f,
            bool depthTest = false)
        {
            InitDebugDrawHelper();
            m_helper.RegisterLine(startPnt, endPnt, color, duration, depthTest);
        }

        public static void AttachLine(
            Transform transform, Vector3 startPnt, Vector3 endPnt, Color color,
            float duration = 0.0f,
            bool depthTest = false)
        {
            InitDebugDrawHelper();
            m_helper.RegisterAttachedLine(transform, startPnt, endPnt, color, duration, depthTest);
        }

        public static void AttachLine(
            Transform startTransform, Vector3 startPnt, Transform endTransform, Vector3 endPnt,
            Color color, float duration = 0.0f, bool depthTest = false)
        {
            InitDebugDrawHelper();
            m_helper.RegisterAttachedLine(
                startTransform, startPnt, endTransform, endPnt, color, duration, depthTest);
        }

        public static void DrawWireCube(
            Vector3 center, Vector3 size, Color color, float duration = 0.0f,
            bool depthTest = false)
        {
            InitDebugDrawHelper();
            m_helper.RegisterWireCube(center, size, color, duration, depthTest);
        }

        public static void DrawWireCube(
            Vector3 center, Vector3 size, Quaternion rotation, Color color, float duration = 0.0f,
            bool depthTest = false)
        {
            InitDebugDrawHelper();
            m_helper.RegisterWireCube(center, size, rotation, color, duration, depthTest);
        }

        public static void AttachWireCube(
            Transform transform, Vector3 center, Vector3 size, Color color, float duration = 0.0f,
            bool depthTest = false)
        {
            InitDebugDrawHelper();
            m_helper.RegisterAttachedWireCube(transform, center, size, color, duration, depthTest);
        }

        public static void DrawText(
            Vector3 position, string text, int size, Color color, float duration = 0.0f)
        {
            InitDebugDrawHelper();
            m_helper.RegisterText(position, text, size, color, duration);
        }

        public static void DrawText(
            Vector3 position, TextFunc textFunc, int size, Color color, float duration = 0.0f)
        {
            InitDebugDrawHelper();
            m_helper.RegisterText(position, textFunc, size, color, duration);
        }

        public static void AttachText(
            Transform transform, Vector3 position, string text, int size, Color color,
            float duration = 0.0f)
        {
            InitDebugDrawHelper();
            m_helper.RegisterAttachedText(transform, position, text, size, color, duration);
        }

        public static void AttachText(
            Transform transform, Vector3 position, TextFunc textFunc, int size, Color color,
            float duration = 0.0f)
        {
            InitDebugDrawHelper();
            m_helper.RegisterAttachedText(transform, position, textFunc, size, color, duration);
        }

        // ----------------------------------------------------------------------

        private static void InitDebugDrawHelper()
        {
            if (m_helper != null)
                return;

            m_helper = GameObject.FindObjectOfType<DebugDrawHelper>();

            if (m_helper != null)
                return;

            GameObject containerGO = new GameObject(kHelperGOName);
            containerGO.hideFlags = HideFlags.HideInHierarchy | HideFlags.HideInInspector;

            GameObject helperGO = new GameObject(kHelperGOName);
            helperGO.transform.SetParent(containerGO.transform, false);

            m_helper = helperGO.AddComponent<DebugDrawHelper>();
        }
    }

} // namespace EAC.Utility.DebugDraw
