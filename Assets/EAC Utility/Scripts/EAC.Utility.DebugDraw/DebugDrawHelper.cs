
namespace EAC.Utility.DebugDraw
{
    // system namespaces
    using System.Collections.Generic;
    using IDisposable = System.IDisposable;
    // unity namespaces
    using UnityEngine.Assertions;
    using UnityEngine.Rendering;
    using UnityEngine;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    // local namespaces
    using TextFunc = DebugDraw.TextFunc;


    internal class DebugDrawHelper
        : MonoBehaviour
    {
        private const int kPreAllocLines = 16;
        private const int kPreAllocWireCubes = 16;
        private const int kPreAllocTexts = 16;

        private bool m_initialized = false;

        private List<LineData> m_lineData;
        private List<LineData> m_lineDataNoDepth;
        private EAC.Utility.ObjectPool<LineData> m_lineDataPool;
        private LineBatch m_lines;
        private LineBatch m_linesNoDepth;

        private List<WireCubeData> m_wireCubeData;
        private List<WireCubeData> m_wireCubeDataNoDepth;
        private EAC.Utility.ObjectPool<WireCubeData> m_wireCubeDataPool;
        private CubeBatch m_cubes;
        private CubeBatch m_cubesNoDepth;

        private List<TextData> m_textData;
        private EAC.Utility.ObjectPool<TextData> m_textDataPool;
        private GUIStyle m_textStyle;

        public void RegisterLine(
            Vector3 startPnt, Vector3 endPnt, Color color, float duration, bool depthTest)
        {
            Initialize();
            LineData data = AllocateLineData(depthTest);
            data.startTransform = null;
            data.startPnt = startPnt;
            data.endTransform = null;
            data.endPnt = endPnt;
            data.color = color;
            data.duration = duration;
        }

        public void RegisterAttachedLine(
            Transform transform, Vector3 startPnt, Vector3 endPnt, Color color, float duration,
            bool depthTest)
        {
            Initialize();
            LineData data = AllocateLineData(depthTest);
            data.startTransform = transform;
            data.startPnt = startPnt;
            data.endTransform = transform;
            data.endPnt = endPnt;
            data.color = color;
            data.duration = duration;
        }

        public void RegisterAttachedLine(
            Transform startTransform, Vector3 startPnt, Transform endTransform, Vector3 endPnt,
            Color color, float duration, bool depthTest)
        {
            Initialize();
            LineData data = AllocateLineData(depthTest);
            data.startTransform = startTransform;
            data.startPnt = startPnt;
            data.endTransform = endTransform;
            data.endPnt = endPnt;
            data.color = color;
            data.duration = duration;
        }

        public void RegisterWireCube(
            Vector3 center, Vector3 size, Color color, float duration, bool depthTest)
        {
            Initialize();
            WireCubeData data = AllocateWireCubeData(depthTest);
            data.transform = null;
            data.rotation = Quaternion.identity;
            data.center = center;
            data.size = size;
            data.color = color;
            data.duration = duration;
        }

        public void RegisterWireCube(
            Vector3 center, Vector3 size, Quaternion rotation, Color color, float duration,
            bool depthTest)
        {
            Initialize();
            WireCubeData data = AllocateWireCubeData(depthTest);
            data.transform = null;
            data.rotation = rotation;
            data.center = center;
            data.size = size;
            data.color = color;
            data.duration = duration;
        }

        public void RegisterAttachedWireCube(
            Transform transform, Vector3 center, Vector3 size, Color color, float duration,
            bool depthTest)
        {
            Initialize();
            WireCubeData data = AllocateWireCubeData(depthTest);
            data.transform = transform;
            data.rotation = Quaternion.identity;
            data.center = center;
            data.size = size;
            data.color = color;
            data.duration = duration;
        }

        public void RegisterText(
            Vector3 position, string text, int size, Color color, float duration)
        {
            Initialize();
            TextData data = AllocateTextData();
            data.transform = null;
            data.position = position;
            data.textFunc = null;
            data.content.text = text;
            data.size = size;
            data.color = color;
            data.duration = duration;
#if UNITY_EDITOR
            data.drawFlag = TextDrawFlag.None;
#else
            data.drawFlag = TextDrawFlag.DrawnOnGizmos;
#endif
        }

        public void RegisterText(
            Vector3 position, TextFunc textFunc, int size, Color color, float duration)
        {
            Initialize();
            TextData data = AllocateTextData();
            data.transform = null;
            data.position = position;
            data.textFunc = textFunc;
            data.content.text = string.Empty;
            data.size = size;
            data.color = color;
            data.duration = duration;
#if UNITY_EDITOR
            data.drawFlag = TextDrawFlag.None;
#else
            data.drawFlag = TextDrawFlag.DrawnOnGizmos;
#endif
        }

        public void RegisterAttachedText(
            Transform transform, Vector3 position, string text, int size, Color color, float duration)
        {
            Initialize();
            TextData data = AllocateTextData();
            data.transform = transform;
            data.position = position;
            data.textFunc = null;
            data.content.text = text;
            data.size = size;
            data.color = color;
            data.duration = duration;
#if UNITY_EDITOR
            data.drawFlag = TextDrawFlag.None;
#else
            data.drawFlag = TextDrawFlag.DrawnOnGizmos;
#endif
        }

        public void RegisterAttachedText(
            Transform transform, Vector3 position, TextFunc textFunc, int size, Color color,
            float duration)
        {
            Initialize();
            TextData data = AllocateTextData();
            data.transform = transform;
            data.position = position;
            data.textFunc = textFunc;
            data.content.text = string.Empty;
            data.size = size;
            data.color = color;
            data.duration = duration;
#if UNITY_EDITOR
            data.drawFlag = TextDrawFlag.None;
#else
            data.drawFlag = TextDrawFlag.DrawnOnGizmos;
#endif
        }

        // ----------------------------------------------------------------------
        #region MonoBehaviour

        protected void Awake()
        {
            Initialize();
        }

        protected void OnGUI()
        {
            OnGUIDrawTexts();
        }

        protected void LateUpdate()
        {
            UpdateAndDrawLines();
            UpdateAndDrawCubes();
            UpdateTexts();
        }

#if UNITY_EDITOR
        protected void OnDrawGizmos()
        {
            Initialize();
            OnDrawGizmosDrawTexts();
        }
#endif

        protected void OnDestroy()
        {
            if (m_lines != null)
            {
                m_lines.Dispose();
                m_lines = null;
            }

            if (m_linesNoDepth != null)
            {
                m_linesNoDepth.Dispose();
                m_linesNoDepth = null;
            }

            if (m_cubes != null)
            {
                m_cubes.Dispose();
                m_cubes = null;
            }

            if (m_cubesNoDepth != null)
            {
                m_cubesNoDepth.Dispose();
                m_cubesNoDepth = null;
            }
        }

        #endregion MonoBehaviour
        // ----------------------------------------------------------------------

        private class LineData
        {
            public Transform startTransform;
            public Vector3 startPnt;
            public Transform endTransform;
            public Vector3 endPnt;
            public Color color;
            public float duration;
        }

        private class WireCubeData
        {
            public Transform transform;
            public Quaternion rotation;
            public Vector3 center;
            public Vector3 size;
            public Color color;
            public float duration;
        }

        [System.Flags]
        private enum TextDrawFlag
        {
            None = 0,
            DrawnOnGUI = 1 << 0,
            DrawnOnGizmos = 1 << 1,
            DrawnAll = DrawnOnGUI | DrawnOnGizmos
        }

        private class TextData
        {
            public Transform transform;
            public Vector3 position;
            public GUIContent content;
            public TextFunc textFunc;
            public int size;
            public Color color;
            public float duration;
            public TextDrawFlag drawFlag;

            public TextData()
            {
                content = new GUIContent();
                drawFlag = TextDrawFlag.None;
            }
        }

        // ======================================================================

        private class LineBatch
            : IDisposable
        {
            public Mesh mesh;
            public Material material;
            public bool dirty;

            private List<Vector3> m_positions;
            private List<Color> m_colors;
            private List<int> m_indices;

            public LineBatch()
            {
                mesh = new Mesh();
                mesh.MarkDynamic();
                mesh.name = "DebugDrawHelper.LineBatch";

                m_positions = new List<Vector3>(2 * kPreAllocLines);
                m_colors = new List<Color>(2 * kPreAllocLines);
                m_indices = new List<int>(2 * kPreAllocLines);
            }

            public void Reset()
            {
                mesh.Clear();
                m_positions.Clear();
                m_colors.Clear();
                m_indices.Clear();
            }

            public void BuildBatch(List<LineData> lineData)
            {
                for (int i = 0, iEnd = lineData.Count; i < iEnd; ++i)
                {
                    LineData data = lineData[i];
                    AddLine(data);
                }

                mesh.SetVertices(m_positions);
                mesh.SetColors(m_colors);
                mesh.SetIndices(m_indices.ToArray(), MeshTopology.Lines, 0);
                dirty = false;
            }

            public void Draw()
            {
                int layer = 0;
                Camera camera = null;
                int submeshIndex = 0;
                MaterialPropertyBlock propertyBlock = null;
                ShadowCastingMode castShadows = ShadowCastingMode.Off;
                bool receiveShadows = false;

                Graphics.DrawMesh(
                    mesh, Vector3.zero, Quaternion.identity, material, layer, camera,
                    submeshIndex, propertyBlock, castShadows, receiveShadows);
            }

            public void Dispose()
            {
                if (mesh != null)
                {
                    GameObject.DestroyImmediate(mesh);
                    mesh = null;
                }

                if (material != null)
                {
                    GameObject.DestroyImmediate(material);
                    material = null;
                }
            }

            // ------------------------------------------------------------------

            private void AddLine(LineData data)
            {
                Vector3 startPnt = data.startPnt;
                Vector3 endPnt = data.endPnt;

                if (data.startTransform != null)
                    startPnt = data.startTransform.TransformPoint(startPnt);

                if (data.endTransform != null)
                    endPnt = data.endTransform.TransformPoint(endPnt);

                m_positions.Add(startPnt);
                m_positions.Add(endPnt);
                m_colors.Add(data.color);
                m_colors.Add(data.color);

                int numVerts = m_positions.Count;
                m_indices.Add(numVerts - 2);
                m_indices.Add(numVerts - 1);
            }
        }

        // ======================================================================

        private class CubeBatch
            : IDisposable
        {
            public Mesh mesh;
            public Material material;
            public bool dirty;

            // XXX TODO: This does not handle the limit of at most 1023 instances that can be
            //           rendered in in a single draw call.
            private Dictionary<Color, List<Matrix4x4>> m_instanceMap;
            private EAC.Utility.ObjectPool<List<Matrix4x4>> m_matrixPool;
            private int m_colorPropID;

            public CubeBatch()
            {
                InitializeMesh();

                m_instanceMap = new Dictionary<Color, List<Matrix4x4>>();
                m_matrixPool = new EAC.Utility.ObjectPool<List<Matrix4x4>>();
                m_colorPropID = Shader.PropertyToID("_Color");
            }

            public void Reset()
            {
                foreach (List<Matrix4x4> matrices in m_instanceMap.Values)
                {
                    matrices.Clear();
                    m_matrixPool.Release(matrices);
                }

                m_instanceMap.Clear();
            }

            public void BuildBatch(List<WireCubeData> cubeData)
            {
                for (int i = 0, iEnd = cubeData.Count; i < iEnd; ++i)
                {
                    WireCubeData data = cubeData[i];
                    AddWireCube(data);
                }

                dirty = false;
            }

            public void Draw()
            {
                ShadowCastingMode castShadows = ShadowCastingMode.Off;
                bool receiveShadows = false;
                int layer = 0;
                Camera camera = null;
                MaterialPropertyBlock properties = new MaterialPropertyBlock();

                foreach (KeyValuePair<Color, List<Matrix4x4>> item in m_instanceMap)
                {
                    properties.SetColor(m_colorPropID, item.Key);
                    Graphics.DrawMeshInstanced(
                        mesh, 0, material, item.Value, properties, castShadows, receiveShadows,
                        layer, camera);
                }
            }

            public void Dispose()
            {
                if (mesh != null)
                {
                    GameObject.DestroyImmediate(mesh);
                    mesh = null;
                }

                if (material != null)
                {
                    GameObject.DestroyImmediate(material);
                    material = null;
                }

                m_instanceMap = null;
            }

            // ------------------------------------------------------------------

            private void InitializeMesh()
            {
                mesh = new Mesh();
                mesh.name = "DebugDrawHelper.CubeBatch";

                List<Vector3> positions = new List<Vector3>(8);
                int[] indices = new int[24];

                positions.Add(new Vector3(0.5f, 0.5f, 0.5f));
                positions.Add(new Vector3(0.5f, 0.5f, -0.5f));
                positions.Add(new Vector3(0.5f, -0.5f, 0.5f));
                positions.Add(new Vector3(0.5f, -0.5f, -0.5f));

                positions.Add(new Vector3(-0.5f, 0.5f, 0.5f));
                positions.Add(new Vector3(-0.5f, 0.5f, -0.5f));
                positions.Add(new Vector3(-0.5f, -0.5f, 0.5f));
                positions.Add(new Vector3(-0.5f, -0.5f, -0.5f));

                indices[0] = 0;
                indices[1] = 1;
                indices[2] = 1;
                indices[3] = 3;
                indices[4] = 3;
                indices[5] = 2;
                indices[6] = 2;
                indices[7] = 0;

                indices[8] = 4;
                indices[9] = 5;
                indices[10] = 5;
                indices[11] = 7;
                indices[12] = 7;
                indices[13] = 6;
                indices[14] = 6;
                indices[15] = 4;

                indices[16] = 0;
                indices[17] = 4;
                indices[18] = 1;
                indices[19] = 5;
                indices[20] = 2;
                indices[21] = 6;
                indices[22] = 3;
                indices[23] = 7;

                mesh.SetVertices(positions);
                mesh.SetIndices(indices, MeshTopology.Lines, 0);
            }

            private void AddWireCube(WireCubeData data)
            {
                List<Matrix4x4> matrices = null;

                if (!m_instanceMap.TryGetValue(data.color, out matrices))
                {
                    matrices = m_matrixPool.Acquire();
                    m_instanceMap.Add(data.color, matrices);
                }

                Matrix4x4 matrix = Matrix4x4.TRS(data.center, data.rotation, data.size);

                if (data.transform != null)
                    matrix = data.transform.localToWorldMatrix * matrix;

                matrices.Add(matrix);
            }
        }

        // ======================================================================

        private void Initialize()
        {
            if (m_initialized)
                return;

            InitializeLineData();
            InitializeWireCubeData();
            InitializeTextData();

            m_lines = new LineBatch();
            Shader lineShader = Shader.Find("EAC.Utility.DebugDraw/Unlit-VertexColor");
            m_lines.material = new Material(lineShader);

            m_linesNoDepth = new LineBatch();
            Shader lineShaderNoDepth
                = Shader.Find("EAC.Utility.DebugDraw/Unlit-VertexColor-NoDepth");
            m_linesNoDepth.material = new Material(lineShaderNoDepth);

            m_cubes = new CubeBatch();
            m_cubes.dirty = false;
            Shader cubeShader = Shader.Find("EAC.Utility.DebugDraw/Unlit-Color-Inst");
            m_cubes.material = new Material(cubeShader);
            m_cubes.material.enableInstancing = true;
            m_cubes.material.SetColor("_Color", Color.red);

            m_cubesNoDepth = new CubeBatch();
            m_cubesNoDepth.dirty = false;
            Shader cubeShaderNoDepth
                = Shader.Find("EAC.Utility.DebugDraw/Unlit-Color-Inst-NoDepth");
            m_cubesNoDepth.material = new Material(cubeShaderNoDepth);
            m_cubesNoDepth.material.enableInstancing = true;
            m_cubesNoDepth.material.SetColor("_Color", Color.red);

            m_textStyle = new GUIStyle();

            m_initialized = true;
        }

        private void InitializeLineData()
        {
            m_lineData = new List<LineData>(kPreAllocLines);
            m_lineDataNoDepth = new List<LineData>(kPreAllocLines);
            m_lineDataPool = new EAC.Utility.ObjectPool<LineData>();
            m_lineDataPool.PreAllocate(kPreAllocLines);
        }

        private void InitializeWireCubeData()
        {
            m_wireCubeData = new List<WireCubeData>(kPreAllocWireCubes);
            m_wireCubeDataNoDepth = new List<WireCubeData>(kPreAllocWireCubes);
            m_wireCubeDataPool = new EAC.Utility.ObjectPool<WireCubeData>();
            m_wireCubeDataPool.PreAllocate(kPreAllocWireCubes);
        }

        private void InitializeTextData()
        {
            m_textData = new List<TextData>(kPreAllocTexts);
            m_textDataPool = new EAC.Utility.ObjectPool<TextData>();
            m_textDataPool.PreAllocate(kPreAllocTexts);
        }

        private LineData AllocateLineData(bool depthTest)
        {
            LineData data = m_lineDataPool.Acquire();

            if (depthTest)
            {
                m_lineData.Add(data);
                m_lines.dirty = true;
            }
            else
            {
                m_lineDataNoDepth.Add(data);
                m_linesNoDepth.dirty = true;
            }

            return data;
        }

        private void ReleaseLineData(LineData data)
        {
            m_lineDataPool.Release(data);
        }

        private WireCubeData AllocateWireCubeData(bool depthTest)
        {
            WireCubeData data = m_wireCubeDataPool.Acquire();

            if (depthTest)
            {
                m_wireCubeData.Add(data);
                m_cubes.dirty = true;
            }
            else
            {
                m_wireCubeDataNoDepth.Add(data);
                m_cubesNoDepth.dirty = true;
            }

            return data;
        }

        private void ReleaseWireCubeData(WireCubeData data)
        {
            m_wireCubeDataPool.Release(data);
        }

        private TextData AllocateTextData()
        {
            TextData data = m_textDataPool.Acquire();
            m_textData.Add(data);

            return data;
        }

        private void ReleaseTextData(TextData data)
        {
            m_textDataPool.Release(data);
        }

        private void UpdateAndDrawLines()
        {
            UpdateLineBatch(m_lines, m_lineData);
            m_lines.Draw();
            UpdateLineData(m_lines, m_lineData);

            UpdateLineBatch(m_linesNoDepth, m_lineDataNoDepth);
            m_linesNoDepth.Draw();
            UpdateLineData(m_linesNoDepth, m_lineDataNoDepth);
        }

        private void UpdateAndDrawCubes()
        {
            UpdateCubeBatch(m_cubes, m_wireCubeData);
            m_cubes.Draw();
            UpdateWireCubeData(m_cubes, m_wireCubeData);

            UpdateCubeBatch(m_cubesNoDepth, m_wireCubeDataNoDepth);
            m_cubesNoDepth.Draw();
            UpdateWireCubeData(m_cubesNoDepth, m_wireCubeDataNoDepth);
        }

        private void UpdateLineBatch(LineBatch lineBatch, List<LineData> lineData)
        {
            if (lineBatch.dirty)
            {
                lineBatch.Reset();
                lineBatch.BuildBatch(lineData);
            }
        }

        private void UpdateLineData(LineBatch lineBatch, List<LineData> lineData)
        {
            int transformCount = 0;

            for (int i = 0, iEnd = lineData.Count; i < iEnd; ++i)
            {
                LineData data = lineData[i];
                data.duration -= Time.deltaTime;

                // count lines with start/end transforms
                if (data.startTransform != null || data.endTransform != null)
                    ++transformCount;

                // replace expired lines with null (will be compacted below)
                if (data.duration < 0.0f)
                {
                    lineData[i] = null;
                    ReleaseLineData(data);
                }
            }

            // compact lineData, removing null entries
            int removeCount = lineData.RemoveAll(data => { return data == null; });

            // if any lines have start/end transforms or were removed the batch must be rebuilt
            if (transformCount > 0 || removeCount > 0)
                lineBatch.dirty = true;
        }

        private void UpdateCubeBatch(CubeBatch cubeBatch, List<WireCubeData> cubeData)
        {
            if (cubeBatch.dirty)
            {
                cubeBatch.Reset();
                cubeBatch.BuildBatch(cubeData);
            }
        }

        private void UpdateWireCubeData(CubeBatch cubeBatch, List<WireCubeData> cubeData)
        {
            int transformCount = 0;

            for (int i = 0, iEnd = cubeData.Count; i < iEnd; ++i)
            {
                WireCubeData data = cubeData[i];
                data.duration -= Time.deltaTime;

                // count cubes with transform
                if (data.transform != null)
                    ++transformCount;

                if (data.duration < 0.0f)
                {
                    cubeData[i] = null;
                    ReleaseWireCubeData(data);
                }
            }

            // compact cubeData, removing null entries
            int removeCount = cubeData.RemoveAll(data => { return data == null; });

            // if any cubes have a transform or were removed the batch must be rebuilt
            if (transformCount > 0 || removeCount > 0)
                cubeBatch.dirty = true;
        }

        private void UpdateTexts()
        {
            for (int i = 0, iEnd = m_textData.Count; i < iEnd; ++i)
            {
                TextData data = m_textData[i];
                data.duration -= Time.deltaTime;

                // XXX TODO: Why is DrawnOnGUI never set??
                if ((data.drawFlag & TextDrawFlag.DrawnOnGizmos) != 0)
                {
                    if (data.duration < 0.0f)
                    {
                        m_textData[i] = null;
                        ReleaseTextData(data);
                    }
                    else if (data.textFunc != null)
                    {
                        data.content.text = data.textFunc();
                    }

#if UNITY_EDITOR
                    data.drawFlag = TextDrawFlag.None;
#else
                    data.drawFlag = TextDrawFlag.DrawnOnGizmos;
#endif
                }
            }

            m_textData.RemoveAll(data => { return data == null; });
        }

        private void OnGUIDrawTexts()
        {
            Camera camera = DebugDraw.GetDebugDrawCamera();

            if (camera == null)
                return;

            for (int i = 0, iEnd = m_textData.Count; i < iEnd; ++i)
            {
                TextData data = m_textData[i];
                DoDrawText(camera, data);

                data.drawFlag |= TextDrawFlag.DrawnOnGUI;
            }
        }

#if UNITY_EDITOR
        private void OnDrawGizmosDrawTexts()
        {
            Camera camera = Camera.current;

            if (!(camera == DebugDraw.GetDebugDrawCamera()
                || camera == SceneView.lastActiveSceneView.camera))
                return;

            if (camera == null)
                return;

            Handles.BeginGUI();
            {
                for (int i = 0, iEnd = m_textData.Count; i < iEnd; ++i)
                {
                    TextData data = m_textData[i];
                    DoDrawText(camera, data);

                    data.drawFlag |= TextDrawFlag.DrawnOnGizmos;
                }
            }
            Handles.EndGUI();
        }
#endif

        private void DoDrawText(Camera camera, TextData data)
        {
            // world space position
            Vector3 positionW = data.position;

            if (data.transform != null)
                positionW = data.transform.TransformPoint(positionW);

            // screen space position
            Vector3 positionS = camera.WorldToScreenPoint(positionW);
            positionS.y = Screen.height - positionS.y;

            m_textStyle.normal.textColor = data.color;
            m_textStyle.fontSize = data.size;
            Rect textRect = new Rect(positionS, m_textStyle.CalcSize(data.content));

            GUI.Label(textRect, data.content, m_textStyle);
        }
    }

} // namespace EAC.Utility.DebugDraw
