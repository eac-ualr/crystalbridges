
namespace EAC.Utility
{
    // system namespaces
    using System.Collections.Generic;
    // unity namespaces
    using UnityEngine;
    using UnityEngine.Assertions;
    using UnityEngine.Events;


    // ==========================================================================
    /// <summary>
    /// Implements a form of pool allocator for class type T.
    /// </summary>

    public class ObjectPool<T> where T : new()
    {
        public int countAllocated
        {
            get
            {
                return m_allocated;
            }

            private set
            {
                m_allocated = value;
            }
        }

        public int countActive
        {
            get
            {
                return m_allocated - m_pool.Count;
            }
        }

        public int countInactive
        {
            get
            {
                return m_pool.Count;
            }
        }

        public int maxInactive
        {
            get
            {
                return m_maxInactive;
            }

            set
            {
                if (value != m_maxInactive)
                {
                    m_maxInactive = value;
                    ShrinkPool();
                }
            }
        }

        private readonly Stack<T> m_pool = new Stack<T>();
        private readonly UnityAction<T> m_onAcquireAction;
        private readonly UnityAction<T> m_onReleaseAction;

        private int m_allocated = 0;
        private int m_maxInactive = System.Int32.MaxValue;

        public ObjectPool(
            UnityAction<T> onAcquireAction = null, UnityAction<T> onReleaseAction = null)
        {
            m_onAcquireAction = onAcquireAction;
            m_onReleaseAction = onReleaseAction;
        }

        public T Acquire()
        {
            T element;

            if (m_pool.Count == 0)
            {
                element = new T();
                ++m_allocated;
            }
            else
            {
                element = m_pool.Pop();
            }

            if (m_onAcquireAction != null)
                m_onAcquireAction(element);

            return element;
        }

        public void Release(T element)
        {
            Assert.IsFalse(
                m_pool.Count > 0 && ReferenceEquals(m_pool.Peek(), element),
                "[ObjectPool.Release] Attempt to release object that is already in the pool!");

            if (m_onReleaseAction != null)
                m_onReleaseAction(element);

            if (m_pool.Count < m_maxInactive)
                m_pool.Push(element);
        }

        public void PreAllocate(int count)
        {
            for (int i = 0; i < count; ++i)
                m_pool.Push(new T());
        }

        // ----------------------------------------------------------------------

        private void ShrinkPool()
        {
            while (m_pool.Count >= m_maxInactive)
                m_pool.Pop();
        }
    }

} // namespace EAC.Utility
